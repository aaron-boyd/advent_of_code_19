#!/usr/bin/python3

import argparse
import matplotlib.pyplot as plt

class Move:
    def __init__(self, raw):
        assert type(raw) == str
        self.direct = raw[0]
        self.dist = int(raw[1:])

    def __repr__(self):
        return f'{self.direct}:{self.dist}'

class Position:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'Point: ({self.x},{self.y})\tDistance: {self.__len__()}'

    def __eq__(self, other):
        if self.x == other.x:
            if self.y == other.y:
                return True
        return False

    def __len__(self):
        return abs(self.x) + abs(self.y)

    def __add__(self, move):
        if move.direct == 'L':
            self.x -= move.dist
        elif move.direct == 'R':
            self.x += move.dist
        elif move.direct == 'U':
            self.y += move.dist
        elif move.direct == 'D':
            self.y -= move.dist

        return Position(self.x, self.y)

    def get_coords(self):
        return self.x, self.y

class Line:

    def __init__(self, pos_1, pos_2):
        self.pos_1 = pos_1
        self.pos_2 = pos_2

    def __repr__(self):
        return f'{self.pos_1} <-> {self.pos_2}'

    def has_intersect(self, other):
        l1_x1, l1_y1 = self.pos_1.get_coords()
        l1_x2, l1_y2 = self.pos_2.get_coords()
        l2_x1, l2_y1 = other.pos_1.get_coords()
        l2_x2, l2_y2 = other.pos_2.get_coords()

        if l1_x1 == l1_x2:
            if l2_y1 == l2_y2:
                if l2_y1 <= max(l1_y1, l1_y2) and l2_y1 >= min(l1_y1, l1_y2):
                    if l1_x1 <= max(l2_x1, l2_x2) and l1_x1 >= min(l2_x1, l2_x2):
                        return Position(l1_x1, l2_y1)

        l1_x1, l1_y1 = other.pos_1.get_coords()
        l1_x2, l1_y2 = other.pos_2.get_coords()
        l2_x1, l2_y1 = self.pos_1.get_coords()
        l2_x2, l2_y2 = self.pos_2.get_coords()

        if l1_x1 == l1_x2:
            if l2_y1 == l2_y2:
                if l2_y1 <= max(l1_y1, l1_y2) and l2_y1 >= min(l1_y1, l1_y2):
                    if l1_x1 <= max(l2_x1, l2_x2) and l1_x1 >= min(l2_x1, l2_x2):
                        return Position(l1_x1, l2_y1)

        return None


def get_paths(file_name):
    with open(file_name, 'r') as fh:
        lines = fh.readlines()
        paths = [[Move(move) for move in line.strip().split(',')] for line in lines]
        assert len(paths) == 2
        return paths

def get_walk(path):
    start = Position(0,0)
    nxt = None
    walk = []
    for move in path:
        nxt = start + move
        walk.append(Line(start, nxt))
        start = nxt
    return walk

def get_coords(lines):
    x = []
    y = []
    for line in lines:
        x.append(line.pos_1.x)
        y.append(line.pos_1.y)

    return x,y

def plot(walk_1, walk_2):
    x_1, y_1 = get_coords(walk_1)
    x_2, y_2 = get_coords(walk_2)
    plt.plot(x_1, y_1, linewidth=1, color='red')
    plt.plot(x_2, y_2, linewidth=1, color='green')
    plt.show()

def main():
    parser = argparse.ArgumentParser(description='path finder calculator')
    parser.add_argument('path_file', help='file with comma seperated paths')
    parser.add_argument('-p', '--plot', help='plot wire diagram', action='store_true')

    args = parser.parse_args()

    path_1, path_2 = get_paths(args.path_file)

    walk_1 = get_walk(path_1)
    walk_2 = get_walk(path_2)

    intersections = list()

    for line_1 in walk_1:
        for line_2 in walk_2:
            intersect = line_1.has_intersect(line_2)
            if intersect:
                intersections.append(intersect)

    intersections = sorted(intersections, key=lambda x: len(x))

    print(intersections[0])

    if args.plot:
        plot(walk_1, walk_2)

if __name__ == '__main__':
    main()
