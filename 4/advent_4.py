#!/usr/bin/python3

# 1. It is a six-digit number.
# 2. The value is within the range given in your puzzle input.
# 3. Two adjacent digits are the same (like 22 in 122345).
# 4. Going from left to right, the digits never decrease; they
#    only ever increase or stay the same (like 111123 or 135679).
# Range: 272091-815432

def check_number(num):
    flag = False
    for i in range(0, len(num) - 1):
        if num[i] == num[i+1]:
            flag = True
            break
    if not flag:
        return False

    for i in range(1,len(num)):
        if num[i] < num[i-1]:
            return False

    return True

def main():
    nums = []
    for i in range(272091, 815432):
        if check_number(str(i)):
            nums.append(i)

    print(len(nums))


if __name__ == '__main__':
    main()
