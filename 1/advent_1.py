#!/usr/bin/python3
modules = [int(line.strip()) for line in open('modules.txt', 'r').readlines()]

total_fuel = 0

for module in modules:
    fuel = module // 3 - 2
    if fuel > 0:
        total_fuel += fuel
        modules.append(fuel)

print(total_fuel)
