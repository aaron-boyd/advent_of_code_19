#!/usr/bin/python3

import argparse

def get_ops(file_name):
    with open(file_name, 'r') as fh:
        text = fh.read()
        ops = [int(num.strip()) for num in text.split(',')]
        return ops

def run(all_ops):
    start = 0
    end = 4
    while end < len(all_ops):
        cmd, oppr_1, oppr_2, dest = all_ops[start:end]
        if cmd == 1:
            all_ops[dest] = all_ops[oppr_1] + all_ops[oppr_2]
        elif cmd == 2:
            all_ops[dest] = all_ops[oppr_1] * all_ops[oppr_2]
        elif cmd == 99:
            break
        else:
            raise Exception(f'Illegal op code {start}\nOp code dump:\n{all_ops}')

        start += 4
        end += 4

    return all_ops

def main():
    parser = argparse.ArgumentParser(description='op code computer')
    parser.add_argument('op_file', help='file with comma seperated op codes')

    args = parser.parse_args()

    clean_ops = get_ops(args.op_file)

    for noun in range(0, 100):
        for verb in range(0, 100):
            all_ops = clean_ops[:]
            all_ops[1] = noun
            all_ops[2] = verb
            try:
                end_ops = run(all_ops)
                if end_ops[0] == 19690720:
                    print(noun, verb, 100 * noun + verb)
            except Exception as e:
                pass

if __name__ == '__main__':
    main()
